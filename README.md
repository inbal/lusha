# Lusha



# Project architecture
Ive created 2 node projects, mongoDB and Message queue(ActiveMQ) for communication.

## Parser project
The parser is for client communication. 
It contains the post '/parse' request that gets url and returns his html - 
- If exsit on Db - returns html from db
- If not - returns mock parser htnl and the url is written to the  Message queue

## Subscriber project
The subscriber handles the ActiveMQ messages. 
For each message:
- If the doesnt exist on db: 
    - Save the url and html (get from mock parser)
    - add url inner url's to the ActiveMQ

## DB - mongo
- Used mongoose as ODM
- Used the _id column as pk - for urls 

## Some notes
- I assume that I could get the same solution by using new promise and async function instead of using message queue
- On same issue - I thought that maybe I could change the publish method to async (and the return will be faster)
- I used the same mongoose models on both project - should upload to npm
- I read about async-await, maybe could replace the promise then
- I would maybe choose other db, that knows to lock row. I had a duplicates issue on the subscriber
    - If the same url will be publish twice, and the second publish wont wait till the first one will save to db, Ill get duplicate exception.
    - I resolved it with catch the duplicate exception.
    - Another option is saveOrUpdate function.
- env variables should be on shared place or on deployment
