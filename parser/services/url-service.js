const urlModel = require('../models');

class URLService {
  constructor () {}

  getUrl(url) {
    return urlModel.findById(url).exec();
  } 
}

module.exports = URLService;