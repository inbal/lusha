const Stomp = require("stomp-client");

const stompClient = new Stomp("127.0.0.1", 61613);

class UrlPublisher {
  constructor () {
    stompClient.connect();
  }

  publish(url) {
      console.log("publish " + url);
      
      stompClient.publish("/queue/urls", JSON.stringify(url));
  } 
}

module.exports = UrlPublisher;