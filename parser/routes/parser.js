var express = require('express');
const app = express();
var router = express.Router();
const URLService = require( "../services/url-service" );
const UrlPublisher = require("../services/url-publisher")
const parse = require('lusha-mock-parser');

const URLServiceInstance = new URLService();
const UrlPublisherInstance = new UrlPublisher();

router.post("/parse", (request, response) => {
  var requestUrl = request.body.url;

  URLServiceInstance.getUrl(requestUrl).then(url => {
    if(url) {
      console.log("not publish " + url);

      response.send(url._doc.html);
    }
    else {
      var parseResult = parse(requestUrl)
      UrlPublisherInstance.publish(requestUrl);
  
      response.send(parseResult.html)
    }
  }).catch(e =>
    log(e));
});

module.exports = router;
