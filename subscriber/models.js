const mongoose = require("mongoose");

const URLSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true,
  },
  html: {
    type: String,
    required: true,
  }
});

const URL = mongoose.model("URL", URLSchema, "URLS");

module.exports = URL;