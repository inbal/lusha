const Stomp = require("stomp-client");
const mongoose = require("mongoose");
const URLService = require( "./url-service" );
const parse = require('lusha-mock-parser');
const URL = require("./models");

const stompClient = new Stomp("127.0.0.1", 61613);
const URLServiceInstance = new URLService();

mongoose.connect('mongodb://localhost:27017/parsedb');
const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

stompClient.connect(function() {
    stompClient.subscribe("/queue/urls", function(body) {
        var url = JSON.parse(body);
        console.log(url);

            URLServiceInstance.isExist(url).then(isExist => {
              if(!isExist) {
                  parsResult = parse(url);
                
                    URLServiceInstance.save(url, parsResult.html).then(() => {
                      console.log("publishing " + url + " links: " + parsResult.links);
                      parsResult.links.forEach (link => stompClient.publish("/queue/urls", JSON.stringify(link)));
                 }).catch(e => {
                     console.log("failed saving " + url + " ex: " + e);
                 })
                } else {
                    console.log(url + " already exist");
             }
        });
    });
});