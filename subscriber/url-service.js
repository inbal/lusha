const urlModel = require('./models');

class URLService {
  constructor () {}

  isExist(url) {
    return urlModel.exists({_id: url});
  }

  save(url, html) {
    const model = new urlModel({
      "_id": url,
      "html": html
    });

    return model.save();
  }
}

module.exports = URLService;